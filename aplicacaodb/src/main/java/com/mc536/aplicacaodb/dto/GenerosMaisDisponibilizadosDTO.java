package com.mc536.aplicacaodb.dto;

public interface GenerosMaisDisponibilizadosDTO {
    String getGenero();
    long getDisponibilizacoes();
}
