package com.mc536.aplicacaodb.dto;

import java.sql.Date;
/*
public class ObrasMaisDisponibilizadasDTO {
    private String nome;
    private String genero;
    private String autor;
    private Date data;
    private long disponibilizacoes;

}
*/

public interface ObrasMaisDisponibilizadasDTO {
    String getNome();
    String getGenero();
    String getAutor();
    String getDataPublicacao();
    long getDisponibilizacoes();
}
