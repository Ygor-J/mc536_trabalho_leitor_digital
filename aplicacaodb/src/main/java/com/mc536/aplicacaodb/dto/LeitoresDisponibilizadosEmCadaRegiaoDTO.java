package com.mc536.aplicacaodb.dto;

public interface LeitoresDisponibilizadosEmCadaRegiaoDTO {
        String getEstado();
        long getLeitores();
}
