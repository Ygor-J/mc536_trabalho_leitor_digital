package com.mc536.aplicacaodb.dto;

public interface EscolaRegiaoDTO {
    String getEstado();
    String getCidade();
    long getNumero_de_escolas();
}
