package com.mc536.aplicacaodb.dto;

public interface EmprestimoDTO {
    long getMedia();
    long getMaximo();
    long getMinimo();
}
