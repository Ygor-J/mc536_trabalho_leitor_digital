package com.mc536.aplicacaodb.dto;

public interface IdadeLeitorDTO {
    long getAge();
    long getQnt_leitores();
}
