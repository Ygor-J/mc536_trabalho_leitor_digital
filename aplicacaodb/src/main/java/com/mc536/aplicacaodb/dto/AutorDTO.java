package com.mc536.aplicacaodb.dto;

public interface AutorDTO {
    String getAutor();
    long getLeituras();
}
