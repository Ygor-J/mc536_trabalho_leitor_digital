package com.mc536.aplicacaodb.dto;

import com.mc536.aplicacaodb.models.Sexo;

public interface SexoDTO {
    String getSexo();
    long getLeitores_emprestados();
}
