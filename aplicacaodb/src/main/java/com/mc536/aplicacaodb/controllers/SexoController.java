package com.mc536.aplicacaodb.controllers;

import com.mc536.aplicacaodb.dto.AutorDTO;
import com.mc536.aplicacaodb.dto.GenerosMaisDisponibilizadosDTO;
import com.mc536.aplicacaodb.dto.LeitoresDisponibilizadosEmCadaRegiaoDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.dto.SexoDTO;
import com.mc536.aplicacaodb.models.Estado;
import com.mc536.aplicacaodb.repositories.AutorRepository;
import com.mc536.aplicacaodb.repositories.GeneroDisponibilizadoRepository;
import com.mc536.aplicacaodb.repositories.LeitorDigitalRepository;
import com.mc536.aplicacaodb.repositories.ObraDisponibilizadaRepository;
import com.mc536.aplicacaodb.repositories.SexoRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sexo")
public class SexoController {
    @Autowired
    private SexoRepository sexoRepository;

    @GetMapping("/leitores")
    public List<SexoDTO> findLeitoresPorSexo() {
        return sexoRepository.findSexoLeitores();
    }

}
