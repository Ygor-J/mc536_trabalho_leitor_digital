package com.mc536.aplicacaodb.controllers;


import com.mc536.aplicacaodb.dto.EscolaRegiaoDTO;
import com.mc536.aplicacaodb.dto.LeitoresDisponibilizadosEmCadaRegiaoDTO;
import com.mc536.aplicacaodb.repositories.EscolaRegiaoRepository;
import com.mc536.aplicacaodb.repositories.LeitorDigitalRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/escola")
public class EscolaRegiaoController {

    @Autowired
    private EscolaRegiaoRepository escolaRegiaoRepository;

    @GetMapping("/regiao")
    public List<EscolaRegiaoDTO> findEscolaPorRegiao() {
        return escolaRegiaoRepository.findEscolaRegiao();
    }
}