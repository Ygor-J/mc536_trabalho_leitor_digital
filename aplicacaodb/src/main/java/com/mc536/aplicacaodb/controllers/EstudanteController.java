package com.mc536.aplicacaodb.controllers;

import com.mc536.aplicacaodb.models.Estado;
import com.mc536.aplicacaodb.models.Estudante;
import com.mc536.aplicacaodb.repositories.EstadoRepository;
import com.mc536.aplicacaodb.repositories.EstudanteRepository;

import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/estudante")
public class EstudanteController {

    @Autowired
    private EstudanteRepository estudanteRepository;

    @GetMapping
    public List<Estudante> findAllEstudantes() {
        return  (List<Estudante>) estudanteRepository.findAll();
    }

    @GetMapping("/teste")
    public List<Long> findTeste(){
        return estudanteRepository.teste();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Estudante> findEstudanteById(@PathVariable(value = "id") long id) {
        Optional<Estudante> estudante = estudanteRepository.findById(id);

        if(estudante.isPresent()) {
            return ResponseEntity.ok().body(estudante.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
