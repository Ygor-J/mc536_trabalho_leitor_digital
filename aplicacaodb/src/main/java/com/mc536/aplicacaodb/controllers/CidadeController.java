package com.mc536.aplicacaodb.controllers;

import com.mc536.aplicacaodb.models.Cidade;
import com.mc536.aplicacaodb.repositories.CidadeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cidade")
public class CidadeController {
    @Autowired
    private CidadeRepository cidadeRepository;

    @GetMapping
    public List<Cidade> findAllCidades() {
        return (List<Cidade>) cidadeRepository.findAll();
    }

    @GetMapping("/regiao")
    public List<String> findAllRegioes() {
        return cidadeRepository.findAllRegioes();
    }
}
