package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.AutorDTO;
import com.mc536.aplicacaodb.dto.EmprestimoDTO;
import com.mc536.aplicacaodb.dto.EscolaRegiaoDTO;
import com.mc536.aplicacaodb.dto.GenerosMaisDisponibilizadosDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.dto.SexoDTO;
import com.mc536.aplicacaodb.models.Escola;
import com.mc536.aplicacaodb.models.LeitorDigital;
import com.mc536.aplicacaodb.models.ObraDisponibilizada;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SexoRepository extends JpaRepository<LeitorDigital, String> {

    @Query(value = "WITH leitor_sexo AS ( SELECT id_leitor, sexo FROM leitorDigital a INNER JOIN estudante b ON a.id_estudante = b.id_estudante ) SELECT sexo, COUNT(*) AS leitores_emprestados FROM leitor_sexo GROUP BY sexo;", nativeQuery = true)
    List<SexoDTO> findSexoLeitores();
}
