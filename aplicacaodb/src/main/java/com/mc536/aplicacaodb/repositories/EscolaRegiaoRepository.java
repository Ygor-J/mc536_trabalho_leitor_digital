package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.EscolaRegiaoDTO;
import com.mc536.aplicacaodb.dto.GenerosMaisDisponibilizadosDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.models.Escola;
import com.mc536.aplicacaodb.models.ObraDisponibilizada;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EscolaRegiaoRepository extends JpaRepository<Escola, String> {

    @Query(value = "WITH escola_regiao AS ( SELECT a.nome, a.cep, a.numero, b.nome AS cidade, c.sigla AS estado FROM escola a LEFT JOIN cidade b ON a.id_cidade = b.id_cidade INNER JOIN estado c ON b.id_estado = c.id_estado ) SELECT estado, cidade, COUNT(*) AS numero_de_escolas FROM escola_regiao GROUP BY estado, cidade ORDER BY estado, cidade;", nativeQuery = true)
    List<EscolaRegiaoDTO> findEscolaRegiao();
}
