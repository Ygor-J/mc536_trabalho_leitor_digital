package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.models.Estudante;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstudanteRepository extends CrudRepository<Estudante, Long> {
    @Query(value = "SELECT id_estudante FROM estudante", nativeQuery = true)
    List<Long> teste();
}