package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.EmprestimoDTO;
import com.mc536.aplicacaodb.dto.EscolaRegiaoDTO;
import com.mc536.aplicacaodb.dto.GenerosMaisDisponibilizadosDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.models.Escola;
import com.mc536.aplicacaodb.models.LeitorDigital;
import com.mc536.aplicacaodb.models.ObraDisponibilizada;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmprestimoRepository extends JpaRepository<LeitorDigital, String> {

    @Query(value = "WITH emprestimos_dias AS ( SELECT DATEDIFF(data_devolucao, data_emprestimo) AS dias_de_emprestimo FROM leitorDigital ) SELECT AVG(dias_de_emprestimo) as media, MAX(dias_de_emprestimo) AS maximo, MIN(dias_de_emprestimo) AS minimo FROM emprestimos_dias;", nativeQuery = true)
    List<EmprestimoDTO> findEmprestimoEstatistica();
}
