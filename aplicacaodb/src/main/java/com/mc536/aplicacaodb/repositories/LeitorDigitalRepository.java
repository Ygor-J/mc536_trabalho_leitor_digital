package com.mc536.aplicacaodb.repositories;


import com.mc536.aplicacaodb.dto.LeitoresDisponibilizadosEmCadaRegiaoDTO;
import com.mc536.aplicacaodb.models.LeitorDigital;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeitorDigitalRepository extends CrudRepository<LeitorDigital, Long> {
    @Query(value = "WITH\n" +
            "\n" +
            "leitores_com_estado AS (  \n" +
            "SELECT\n" +
            "\n" +
            "leitorDigital.id_leitor,\n" +
            "leitorDigital.id_escola,\n" +
            "escola.nome AS nome_escola,\n" +
            "escola.id_cidade,\n" +
            "cidade.nome AS cidade,\n" +
            "estado.sigla AS estado\n" +
            "\n" +
            "FROM leitorDigital\n" +
            "LEFT JOIN escola on leitorDigital.id_escola = escola.id_escola\n" +
            "LEFT JOIN cidade ON escola.id_cidade = cidade.id_cidade\n" +
            "LEFT JOIN estado ON cidade.id_estado = estado.id_estado\n" +
            ")\n" +
            "-- Mostra o número de leitores digitais disponibilizados em cada região\n" +
            "SELECT estado, COUNT(DISTINCT(id_leitor)) AS leitores FROM leitores_com_estado GROUP By estado;", nativeQuery = true)
    List<LeitoresDisponibilizadosEmCadaRegiaoDTO> findLeitoresDisponibilizadosEmCadaRegiao();
}