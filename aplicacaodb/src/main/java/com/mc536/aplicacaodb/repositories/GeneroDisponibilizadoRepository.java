package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.GenerosMaisDisponibilizadosDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.models.ObraDisponibilizada;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneroDisponibilizadoRepository extends JpaRepository<ObraDisponibilizada, String> {

    @Query(value = "WITH id_obra_disponivel AS ( SELECT id_obra, COUNT(*) AS disponibilizacoes FROM obraDisponibilizada GROUP BY id_obra ), livros_disponibilizados AS ( SELECT b.nome, b.genero, b.autor, b.data_publicacao, a.disponibilizacoes FROM id_obra_disponivel a INNER JOIN obraLiteraria b ON a.id_obra = b.id_obra ) SELECT genero, COUNT(*) AS disponibilizacoes FROM livros_disponibilizados GROUP BY genero ORDER BY disponibilizacoes DESC;", nativeQuery = true)
    List<GenerosMaisDisponibilizadosDTO> findGenerosMaisDisponibilizadas();
}
