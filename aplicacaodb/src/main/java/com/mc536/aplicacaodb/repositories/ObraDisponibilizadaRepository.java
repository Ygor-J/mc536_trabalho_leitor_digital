package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.models.ObraDisponibilizada;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ObraDisponibilizadaRepository extends JpaRepository<ObraDisponibilizada, String> {

    @Query(value = "WITH\n" +
            "\n" +
            "id_obra_disponivel AS (\n" +
            "  \tSELECT\n" +
            "  \n" +
            "\tid_obra,\n" +
            "\tCOUNT(*) AS disponibilizacoes\n" +
            "\n" +
            "\tFROM obraDisponibilizada\n" +
            "\tGROUP BY id_obra\n" +
            ")\n" +
            "\n" +
            "SELECT \n" +
            " \n" +
            "b.nome,\n" +
            "b.genero,\n" +
            "b.autor,\n" +
            "b.data_publicacao AS dataPublicacao,\n" +
            "a.disponibilizacoes\n" +
            " \n" +
            "FROM id_obra_disponivel a\n" +
            "INNER JOIN obraLiteraria b ON a.id_obra = b.id_obra\n" +
            "ORDER BY disponibilizacoes DESC;", nativeQuery = true)
    List<ObrasMaisDisponibilizadasDTO> findObrasMaisDisponibilizadas();
    /* 
    @Query(value =
    "SELECT \n" +
    " \n" +
    "'Oi' AS nome,\n" +
    "'genero' AS genero,\n" +
    "'atu' AS autor,\n" +
    "'2022-09-01' AS data,\n" +
    "'1' AS disponibilizacoes;", nativeQuery = true)
    List<ObrasMaisDisponibilizadasDTO> findObrasMaisDisponibilizadas();
    */
//    List<EstudanteDTO> findAlunosPorNome();
}
