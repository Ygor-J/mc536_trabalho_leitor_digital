package com.mc536.aplicacaodb.repositories;


import com.mc536.aplicacaodb.models.Estado;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoRepository extends CrudRepository<Estado, Long> {
    @Query(value = "SELECT id_estado FROM estado", nativeQuery = true)
    List<Long> teste();
}



