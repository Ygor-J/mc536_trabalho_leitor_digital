package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.models.Cidade;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CidadeRepository extends CrudRepository<Cidade, Long> {
    @Query(value = "SELECT estado.regiao FROM cidade INNER JOIN estado ON cidade.id_estado = estado.id_estado", nativeQuery = true)
    List<String> findAllRegioes();
}
