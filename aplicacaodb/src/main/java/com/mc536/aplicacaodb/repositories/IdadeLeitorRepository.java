package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.GenerosMaisDisponibilizadosDTO;
import com.mc536.aplicacaodb.dto.IdadeLeitorDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.models.LeitorDigital;
import com.mc536.aplicacaodb.models.ObraDisponibilizada;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdadeLeitorRepository extends JpaRepository<LeitorDigital, String> {

    @Query(value = "WITH leitor_estudante_idade AS ( SELECT a.id_leitor, b.id_estudante, b.data_nascimento, TIMESTAMPDIFF(YEAR, b.data_nascimento, CURDATE()) AS Age FROM leitorDigital a INNER JOIN estudante b ON a.id_estudante = b.id_estudante ) SELECT age, COUNT(id_leitor) AS qnt_leitores FROM leitor_estudante_idade GROUP BY age ORDER BY qnt_leitores DESC;", nativeQuery = true)
    List<IdadeLeitorDTO> findIdadeLeitor();
}