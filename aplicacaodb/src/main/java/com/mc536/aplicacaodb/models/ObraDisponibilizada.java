package com.mc536.aplicacaodb.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "obraDisponibilizada")
public class ObraDisponibilizada {
    //TODO(ID duplo)
    @Id
    private long id_obra;
    private long id_leitor;

    public long getId_obra() {
        return id_obra;
    }

    public void setId_obra(long id_obra) {
        this.id_obra = id_obra;
    }

    public long getId_leitor() {
        return id_leitor;
    }

    public void setId_leitor(long id_leitor) {
        this.id_leitor = id_leitor;
    }
}
