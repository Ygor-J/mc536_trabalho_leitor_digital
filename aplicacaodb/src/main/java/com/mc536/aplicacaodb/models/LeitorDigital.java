package com.mc536.aplicacaodb.models;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "leitorDigital")
public class LeitorDigital {
    @Id
    private long id_leitor;
    private Date data_emprestimo;
    private Date data_devolucao;
    private Date data_entrada;
    private long id_escola;
    private long id_estudante;

    public long getId_leitor() {
        return id_leitor;
    }

    public void setId_leitor(long id_leitor) {
        this.id_leitor = id_leitor;
    }

    public Date getData_emprestimo() {
        return data_emprestimo;
    }

    public void setData_emprestimo(Date data_emprestimo) {
        this.data_emprestimo = data_emprestimo;
    }

    public Date getData_devolucao() {
        return data_devolucao;
    }

    public void setData_devolucao(Date data_devolucao) {
        this.data_devolucao = data_devolucao;
    }

    public Date getData_entrada() {
        return data_entrada;
    }

    public void setData_entrada(Date data_entrada) {
        this.data_entrada = data_entrada;
    }

    public long getId_escola() {
        return id_escola;
    }

    public void setId_escola(long id_escola) {
        this.id_escola = id_escola;
    }

    public long getId_estudante() {
        return id_estudante;
    }

    public void setId_estudante(long id_estudante) {
        this.id_estudante = id_estudante;
    }
}