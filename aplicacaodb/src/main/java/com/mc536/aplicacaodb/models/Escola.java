package com.mc536.aplicacaodb.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "escola")
public class Escola {
    @Id
    private long id_escola;
    private String nome;
    private String cep;
    private long numero;
    private long id_cidade;

    public long getId_escola() {
        return id_escola;
    }

    public void setId_escola(long id_escola) {
        this.id_escola = id_escola;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public long getId_cidade() {
        return id_cidade;
    }

    public void setId_cidade(long id_cidade) {
        this.id_cidade = id_cidade;
    }
}
