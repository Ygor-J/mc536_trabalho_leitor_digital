package com.mc536.aplicacaodb.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estado")
public class Estado {
    @Id
    private long id_estado;
    private String sigla;
    private String regiao;
    private long numero_habitantes;

    @Override
    public String toString() {
        return "Estado{" +
                "id=" + id_estado +
                ", sigla='" + sigla + '\'' +
                ", regiao='" + regiao + '\'' +
                ", numero_habitantes=" + numero_habitantes +
                '}';
    }

    public long getId_estado() {
        return id_estado;
    }

    public void setId_estado(long id_estado) {
        this.id_estado = id_estado;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getRegiao() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    public long getNumero_habitantes() {
        return numero_habitantes;
    }

    public void setNumero_habitantes(long numero_habitantes) {
        this.numero_habitantes = numero_habitantes;
    }
}
