# Aplicação em SpringBoot - LeitoresDigitais MC536 - UNICAMP

## Objetivo
A ideia é promover uma aplicação que funcione como uma API para o envio de requisições para um banco de dados. Utilizando Spring Boot e principalmente DTO (Data Transfer Object), foi possível montar a estrutura inicial de um backend de requisições. Abaixo estão listados os endpoints, sendo razoavelmente fácil ampliá-los para novos contextos ou novas tabelas, por exemplo.

## Como rodar a aplicação
### Primeira parte
Primeiramente, é necessário ter alguns softwares. O banco de dados foi hospedado localmente por simplicidade, então para fazer o teste da aplicação será necessário instalar o MySQL Server. Segue abaixo os softwares necessário e suas versões mínimas, ou seja, a aplicação não rodará com versões mais antigas:

1. MySQL Server: 8.0.31
2. openjdk 17.0.5
3. IDE qualquer (IntelliJ ou Vscode)
4. Postman (opcional)

*Observações*:
 - Versões mais antigas do que a versão 8 do MySQL não rodam Common Table Expressions (CTE) e a maior parte das queries utiliza CTE.
 - A aplicação funciona com JDK 11, mas não é o ideal.
 - Alguma IDE é necessário para buildar o projeto, já que não há um executável.
 - O postman é uma API que facilitar a chamada de requisições em outras APIs.
 - De preferência, faça o teste em um ambiente linux. O MySql se comporta um pouco diferente no Windows.
 > On Linux MySQL, case-sensitive table and column names are usually the default, while case-insensitive used to be the default on Windows, but the installer now asks about this during setup.


 ### Segunda parte
 Agora é necessário criar o banco locamente e populá-lo. Os arquivos de criação e de popular estão na pasta na pasta /populando.  
 Primeiro, crie um usuário no MySQL. Faça o login com seu usuário. Nesse caso, foi feito com o usuário root.
 
 ```mysql -u root -p```

 Depois, dentro do mysql você pode rodar o comando ```source file_path```, substituindo file_path pelo caminho do arquivo sql, assim você pode rodar cada query rapidamente.
 Rode primeiro a query de criação de tabela. Depois as queries de popular, deixando a de obras disponibilizadas por último, pois é necessário que as outras tabelas existam para manter a **integridade referencial** na hora de popular ela.
 Ordem:  
 1. ```criando_tabelas.sql```
 2. qualquer uma menos ```populando_obraDisponibilizada.sql```
 3. ```populando_obraDisponibilizada.sql``` 

 Após popular, coloque as credencias do seu usuário no arquivo ```aplicacaodb/src/main/resources/application.properties```, além de especificar na url qual o nome do banco de dados usado. No caso, está setado como "testes".  
 Credencias de exemplo:
 ![credencias](./resource_readme/credenciais.png)

## Uso
Primeiro, fazer o build e rodar a aplicação.  
![run](./resource_readme/run.png)

Usando o terminal ou o postman para fazer uma requisição.  
Terminal:
![terminal](./resource_readme/terminal.png)  

Postman:
![postman](./resource_readme/postman.png)  



## Endpoints: 

### /cidade
*Retorna um join da tabela cidade com a tabela estado. Informações das cidades.*

### /cidade/regiao
*Retorna os nomes das regiões do Brasil*

### /estado
*Retorna tabela estado*

### /estado/{id}
*Retorna as informações do estado especificado pelo id*

### /obra_disponibilizada/obras_mais_disponibilizadas/
*Retorna as obras as obras e a quantidade de vezes que forma disponibilizadas nos leitores*

### /leitor/leitores_regiao
*Retornar o número de leitores disponibilizados em cada estado*

### /estudante
*Retorna tabela estudante*

### /estudante/{id}
*Retorna as informações do estudante especificado pelo id*

### /genero/disponibilizacoes
*Retorna a distribuição das disponibilizações do gêneros dos livros*

### /escola/regiao
*Retorna a distrbuição das escolas inseridas no projeto por região*

### /idade/leitores
*Retorna a quantidade de leitores que cada idade pegou emprestado*

### /emprestimo/estatistica
*Estatísticas do tempo de empréstimo dos leitores*

### /autor/leituras
*Leituras por autor*

### /sexo/leitores
*Empréstimo de leitores por sexo*