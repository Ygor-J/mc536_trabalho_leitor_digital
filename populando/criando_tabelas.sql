CREATE TABLE estado (
  id_estado 			int(2) NOT NULL, 
  sigla 				varchar(2) not NULL, 
  regiao 				varchar(14) NOT NULL, 
  numero_habitantes 	int(9) UNSIGNED, 
  
  primary key (id_estado)
);

CREATE TABLE cidade (
  id_cidade 			int(4) NOT NULL, 
  nome 					varchar(30) not null, 
  numero_habitantes 	int(8) UNSIGNED, 
  id_estado 			int(2) NOT NULL,
  
  primary key (id_cidade),
  FOREIGN key (id_estado) REFERENCES estado(id_estado)
); 

CREATE TABLE escola (
  id_escola 			int(6) UNSIGNED NOT NULL, 
  nome 					varchar(50) Not Null,
  CEP 					varchar(10),
  numero 				int(4), 
  id_cidade 			int(5) NOT NULL,
  
  primary key (id_escola),
  FOREIGN key (id_cidade) REFERENCES cidade(id_cidade)
);

CREATE TABLE estudante (
  id_estudante 		int(8) UNSIGNED NOT NULL, 
  nome 				varchar(50) not null, 
  CPF 				varchar(12) NOT NULL, 
  data_nascimento 	date NOT NULL, 
  sexo 				ENUM('H','M','I') NOT NULL, 
  id_escola 		int(6) UNSIGNED NOT NULL, 
  
  PRIMARY KEY (id_estudante),
  FOREIGN key (id_escola) REFERENCES escola(id_escola)
); 

create table leitorDigital (
  id_leitor 			int(8) UNSIGNED NOT NULL,
  data_emprestimo 		date,
  data_devolucao		date,
  data_entrada 			date NOT NULL, 
  id_escola				int(6) UNSIGNED NOT NULL,
  id_estudante			int(8) UNSIGNED NOT NULL,
  
  PRIMARY KEY (id_leitor),
  FOREIGN Key (id_escola) REFERENCES escola(id_escola),
  FOREIGN KEY (id_estudante) REFERENCES estudante(id_estudante)
);

CREATE TABLE obraLiteraria (
  id_obra 			int UNSIGNED NOT NULL, 
  nome 				varchar(70) NOT NULL, 
  genero 			varchar(18), 
  autor 			varchar(50) NOT NULL, 
  data_publicacao 	date,
  
  PRIMARY KEY (id_obra)
); 

CREATE TABLE obraDisponibilizada (
  id_obra 			int UNSIGNED NOT NULL, 
  id_leitor 		int UNSIGNED NOT NULL, 
  
  PRIMARY KEY (id_obra, id_leitor),
  FOREIGN key (id_obra) REFERENCES obraLiteraria(id_obra),
  FOREIGN key (id_leitor) REFERENCES leitorDigital(id_leitor)
);
