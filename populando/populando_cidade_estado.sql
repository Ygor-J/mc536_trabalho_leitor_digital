INSERT into estado VALUES
(1, 'BA', 'Nordeste', 14985284),
(2, 'GO', 'Centro Oeste', 7206589),
(3, 'PA', 'Norte', 8777124),
(4, 'PR', 'Sul', 11597484),
(5, 'SP', 'Sudeste', 46649132);

INSERT INTO cidade VALUES
(1, 'Salvador', 2900319, 1),
(2, 'Feira de Santana', 624107, 1),
(3, 'Goiânia', 1555626, 2),
(4, 'Aparecida de Goiânia', 601844, 2),
(5, 'Belém', 1506420, 3),
(6, 'Ananindeua', 540410, 3),
(7, 'Curitiba', 1963726, 4),
(8, 'Londrina', 580870, 4),
(9, 'São Paulo', 12396372, 5),
(10, 'Guarulhos', 1404694, 5);

SELECT * from cidade;